Real Time Face Recognition by DataBucket Solutions

 Realtime Face Recognition app using:
 - [face-api.js](https://github.com/justadudewhohacks/face-api.js)
 - [TensorflowJS](https://github.com/tensorflow/tfjs)
 - [VueJS](https://github.com/vuejs/vue)
 - [NuxtJS](https://github.com/nuxt/nuxt.js/)
 - [VuetifyJS](https://github.com/vuetifyjs/vuetify)
 - [ExpressJS](https://github.com/expressjs/expressjs.com)
 - [Docker](https://github.com/docker)
 
 This is not an AI project, but merely an engineering one, trying to utilize face-api.js for performing Face Recognition in the browser and 
 engineer a software solution that will take care of application aspects like:
 
User Registration
Models loading
Image upload, resizing and deleting
Using camera (taking and resizing photos, face recognition)
Training (Face landmarks/descriptors extraction) and storing models

Using the following technology stack:

VueJS / NuxtJS / VuetifyJS — Frontend (simply to look nice)
NodeJS / ExpressJS — Backend (Static content & API handlers)
PM2 — Clustering (multi process support — one per CPU core)

If you want to know more about the face-api.js library, I recommend start with Vincent’s post.
https://itnext.io/face-api-js-javascript-api-for-face-recognition-in-the-browser-with-tensorflow-js-bcc2a6c4cf07

Models

We will be using the following models:
Tiny Face Detector — 190 KB — for face detection (rectangles of faces)
68 Point Face Landmark Detection Models — 80 kb — for face landmark extraction (eyes, eyebrows, nose, lips, etc)
Face Recognition Model — 6.2 MB
The total size of all models is less than 6.5 MB.

The application is using NUXT.JS with SSR (server-side rendering) and it following the its default directory structure convention.

The application in development mode will split the SERVER in two distincts processes:
/server/index.js — for static content (frontend) — listening on port 3000
/api/index.js — for API calls (backend) — listening on port 3001

npm run dev
npm run api


# Install
Run `sudo npm i` in the root folder

# Run

## Development mode
1. Run `npm run api` for starting the API server 
2. Run `npm run dev` for starting in development mode

## Production mode
1. Run `npm run build` for building in production mode
2. Run `npm run start` for starting in production mode

# Deploy

or us the build script with your own docker hub username and image name:

- `./build.sh`


or use the release.sh script with your own docker hub username and image name:
- `./release.sh`